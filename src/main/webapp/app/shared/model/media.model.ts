export interface IMedia {
  filePath?: string;
  fileName?: string;
}

export class Media implements IMedia {
  constructor(public filePath?: string, public fileName?: string) {}
}
