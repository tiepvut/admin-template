import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MediasService } from 'app/account/medias/medias.service';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { IMedia } from 'app/shared/model/media.model';

@Component({
  selector: 'jhi-medias',
  templateUrl: './medias.component.html',
  styleUrls: ['./medias.component.scss']
})
export class MediasComponent implements OnInit {
  media?: IMedia[];
  error = false;
  success = false;

  mediaForm = this.fb.group({
    file: File
  });

  constructor(protected mediaService: MediasService, private fb: FormBuilder) {}

  loadAll(): void {
    this.mediaService.query().subscribe((res: HttpResponse<IMedia[]>) => (this.media = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
  }

  private createInputData(): FormData {
    const inputData = new FormData();
    inputData.append('file', this.mediaForm.get(['file'])!.value);
    return inputData;
  }

  onFileImageChange(event: any): void {
    if (event.target.files.length > 0) {
      const fileContent = event.target.files[0];
      this.mediaForm.patchValue({
        file: fileContent
      });
    }
  }

  protected subscribeToSaveResponse(result: Observable<{}>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  save(): void {
    const inputData = this.createInputData();
    this.subscribeToSaveResponse(this.mediaService.save(inputData));
  }

  protected onSaveSuccess(): void {
    this.success = true;
    this.loadAll();
    // this.previousState();
  }

  protected onSaveError(): void {
    this.error = true;
  }

  previousState(): void {
    window.history.back();
  }
}
