import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { MediasComponent } from './medias.component';
import { Authority } from 'app/shared/constants/authority.constants';

export const mediasRoute: Route = {
  path: 'medias',
  component: MediasComponent,
  data: {
    authorities: [Authority.USER],
    pageTitle: 'global.menu.account.medias'
  },
  canActivate: [UserRouteAccessService]
};
