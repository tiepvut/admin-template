import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IMedia } from 'app/shared/model/media.model';
import { createRequestOption } from 'app/shared/util/request-util';

type EntityArrayResponseType = HttpResponse<IMedia[]>;

@Injectable({ providedIn: 'root' })
export class MediasService {
  public resourceUrl = SERVER_API_URL + 'api/account/medias';
  constructor(private http: HttpClient) {}

  save(file: FormData): Observable<{}> {
    return this.http.post(SERVER_API_URL + 'api/account/medias', file);
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMedia[]>(this.resourceUrl, { params: options, observe: 'response' });
  }
}
